import httpClient from "../../helpers/httpClient";
import {buildQueryJs} from "../../helpers/common";


export const getEverything = async(data = {}) => {
    let endpoint = `${httpClient.api.GET_EVERYTHING}?${buildQueryJs(data)}`;
    const res = await getApiNormal(endpoint);
    return res.data;
}

export const getApiNormal = (url) => {
    return httpClient.get(url, {});
}
