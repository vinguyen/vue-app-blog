import {Fetch} from "./fetch";
import listApi from "../configs/api/listApi";


const httpClient = new Fetch();
httpClient.api = listApi;
export default httpClient;
