import axios from 'axios';

const getClient = (baseUrl = null) => {
  const options = {
    baseURL: baseUrl
  };

  const client = axios.create(options);

  client.interceptors.request.use(
    requestConfig => requestConfig,
    (requestError) => {
      return Promise.reject(requestError);
    }
  );

  client.interceptors.response.use(
    response => response,
    (error) => {
      return Promise.reject(error);
    }
  );

  return client;

};

class Fetch {
  constructor(baseUrl = process.env.VUE_APP_URL_API) {
    this.client = getClient(baseUrl);
  }

  get(url, conf = {}) {
    return this.client.get(url, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error));
  }

  post(url, data = {}, conf = {}) {
    return this.client.post(url, data, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error));
  }

  allResponse (response) {
    let res = false;
    if (response.data.code === 0) {
      res = JSON.parse(JSON.stringify(response.data.data));
    }
    return res;
  }

}

export { Fetch };
