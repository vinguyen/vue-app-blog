import qs from "qs";

export const buildQueryJs = (data) => {
    return qs.stringify(data);
};
